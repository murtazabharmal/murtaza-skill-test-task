<?php

use Illuminate\Database\Seeder;

class AppointmentsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('appointments')->delete();
        
        \DB::table('appointments')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Murtaza',
                'email' => 'murtazabharmal717.mb@gmail.com',
                'phone' => '886-615-2251',
                'appointment_date' => '2018-07-02T12:13:00.000Z',
                'appointment_time' => NULL,
                'created_at' => '2018-07-26 12:13:49',
                'updated_at' => '2018-07-26 12:13:49',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Murtaza',
                'email' => 'murtazabharmal7.mb@gmail.com',
                'phone' => '886-615-2251',
                'appointment_date' => '2018-07-02T12:13:00.000Z',
                'appointment_time' => NULL,
                'created_at' => '2018-07-26 12:27:12',
                'updated_at' => '2018-07-26 12:27:12',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Murtaza',
                'email' => 'murtazabharmal7.mb1@gmail.com',
                'phone' => '886-615-2251',
                'appointment_date' => '2018-07-02T12:13:00.000Z',
                'appointment_time' => NULL,
                'created_at' => '2018-07-26 12:27:22',
                'updated_at' => '2018-07-26 12:27:22',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Murtaza',
                'email' => 'murtazabharmal7.mb2@gmail.com',
                'phone' => '886-615-2251',
                'appointment_date' => '2018-07-20T12:13:00.000Z',
                'appointment_time' => NULL,
                'created_at' => '2018-07-26 12:27:33',
                'updated_at' => '2018-07-26 12:27:33',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Jaskaran',
                'email' => 'murtazabharmal7.mb2@gmail.com',
                'phone' => '886-615-2251',
                'appointment_date' => '2018-07-20T12:13:00.000Z',
                'appointment_time' => NULL,
                'created_at' => '2018-07-26 12:27:43',
                'updated_at' => '2018-07-26 12:27:43',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'James',
                'email' => 'jamestyler@gmail.com',
                'phone' => '886-615-2251',
                'appointment_date' => '2018-07-20T12:13:00.000Z',
                'appointment_time' => NULL,
                'created_at' => '2018-07-26 12:28:31',
                'updated_at' => '2018-07-26 12:28:31',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'kevin',
                'email' => 'kevinhart@gmail.com',
                'phone' => '985-484-5685',
                'appointment_date' => '2018-07-20T12:13:00.000Z',
                'appointment_time' => NULL,
                'created_at' => '2018-07-26 12:29:04',
                'updated_at' => '2018-07-26 12:29:04',
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Akram',
                'email' => 'akramchauhan@gmail.com',
                'phone' => '985-484-5685',
                'appointment_date' => '2018-07-16T12:13:00.000Z',
                'appointment_time' => NULL,
                'created_at' => '2018-07-26 12:29:41',
                'updated_at' => '2018-07-26 12:29:41',
            ),
        ));
        
        
    }
}